---
title: Technical Test for Blablacar - Mower simulation
---

## Overview
This project implements the mower simulation.
To run a simulation, you need to used an input file constructed as requested in the technical test specification.
At the end of the simulation, the application will generate the simulation results as an output file (available inside the same folder as your <input_file>).

We're using multiprocessing to run multiple mowers simultaneously in order to speed up the overall execution time.
To check the current position of the multiple running mowers, we have a shared state using `multiprocessing.Manager()` and `manager.list()`.
The `manager.list()` contains in realtime the current position of every mowers running on the lawn.
We're using a `multiprocessing.Lock()` to avoid concurrent access to this shared state.

## Install

#### Prerequisites
- Python 3.7

#### Install requirements
```bash
  pip install -r requirements.txt
```

## How to run it?

#### Usage
To run a simulation, you need to provide an input file as following:
```
  usage: app.py <inputfile>
```
#### Example
```bash
  python app.py 'input_files/input'
```

#### Input files
You can add your input files inside the directory `input_files/`.
See input file [example](./input_files/input)

#### Output files
When the simulation is terminated, the application generates an output file containing the final simulation results.
You can find the output file inside the same folder as your <input_file>, with a suffix: `_output_<yyyymmddHMS>`.
Note that for each simulation will generate a new output file, even if you run several simulations with the same input time.

## Run tests
```bash
  python -m unittest discover -s tests
```

## Next steps
- Move from the `app.py` the parsing logic to a new specific file
- Add a Makefile
- Add Typechecking using `mypy`
- Add dependency and virtualenv management using `poetry`
- Add linting using `flake8`
- Add code autoformatting using `black` and `isort`
- Checking docstring using `pydocstyle`
- Add performance profiling using `pyinstrument`
- Add Docker support
