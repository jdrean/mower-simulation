import multiprocessing
from structlog import get_logger

from domain.lawn import Lawn
from domain.instruction import Instruction
from domain.mower import Mower

logger = get_logger(__name__)

def run_simulation(lawn: Lawn) -> list:
    """Run the Mower simulation
    :param lawn: Lawn object containing the mowers
    """
    manager = multiprocessing.Manager()
    list_mowers_positions = manager.list()
    list_mowers_final_positions = manager.dict()
    lock = multiprocessing.Lock()
    jobs = []
    for i, mower in enumerate(lawn.list_mowers):
        list_mowers_positions.append(mower.get_coordinates())
        jobs.append(multiprocessing.Process(target=run_mower, args=(i, mower, list_mowers_positions, list_mowers_final_positions, lawn, lock)))

    for job in jobs:
        job.start()

    for job in jobs:
        job.join()

    list_mowers_final_positions_sorted = dict(sorted(list_mowers_final_positions.items()))
    return list_mowers_final_positions_sorted

def run_mower(mower_id: int, mower: Mower, list_mowers_positions: list, list_mowers_final_positions: dict, lawn: Lawn, lock):
    """Run a Mower on the lawn
    :param mower_id: Lawn object containing the mowers
    :param mower: Mower object
    :param list_mowers_positions: Shared list between the process containing the current position of each mower
    :param list_mowers_final_positions: Shared dict containing the final position and orientation of each mower
    :param lawn: Lawn object
    :param lock: A multiprocessing Lock to handle concurrent access to shared variables
    """
    for instruction in mower.instructions:
        if instruction == Instruction.LEFT:
            old_orientation = mower.orientation.get_cardinal_direction()
            new_orientation = mower.turn_left()
            logger.debug("Mower turned left", mower_id=mower_id, old_orientation=old_orientation, new_orientation=new_orientation.get_cardinal_direction())
        elif instruction == Instruction.RIGHT:
            old_orientation = mower.orientation.get_cardinal_direction()
            new_orientation = mower.turn_right()
            logger.debug("Mower turned right", mower_id=mower_id, old_orientation=old_orientation, new_orientation=new_orientation.get_cardinal_direction())
        elif instruction == Instruction.FORWARD:
            new_coordinates = mower.get_forward_coordinates()
            lock.acquire()
            if lawn.is_inside_lawn(new_coordinates[0], new_coordinates[1]) and new_coordinates not in list_mowers_positions:
                old_position = mower.get_coordinates()
                list_mowers_positions.remove(old_position)
                mower.set_coordinates(new_coordinates[0], new_coordinates[1])
                list_mowers_positions.append(new_coordinates)
                lock.release()
                logger.debug("Mower moved forward", mower_id=mower_id, old_position=old_position, new_position=new_coordinates)
            else:
                logger.debug("Unable to proceed the requested instruction. Coordinates outside the lawn or new coordinates used", mower_id=mower_id, failed_position=new_coordinates)

    list_mowers_final_positions[mower_id] = mower.dump_output()
