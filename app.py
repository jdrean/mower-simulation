import sys
from os import listdir
from os.path import isfile, join
from datetime import datetime
from structlog import get_logger

from domain.lawn import Lawn
from service.simulation import run_simulation


logger = get_logger(__name__)

def run(input_file_path: str) -> None:
    """Run the Mower simulation
    :param input_file_path: input file path
    """
    lawn = create_lawn_from_input_file(input_file_path)
    simulation_starting_date = datetime.now()
    logger.info("Starting the simulation...", simulation_starting_date=simulation_starting_date, simulation_input_file=input_file_path)
    res = run_simulation(lawn)
    simulation_ending_date = datetime.now()
    duration_ms = (simulation_starting_date - simulation_ending_date).total_seconds() * 1000
    logger.info("Simulation completed.", simulation_starting_date=simulation_starting_date, duration_ms=duration_ms)
    logger.info("Generating output file...")
    generating_output_file(res, input_file_path, simulation_starting_date)
    logger.info("Output file generated.")


def create_lawn_from_input_file(input_file_path: str) -> Lawn:
    """Create a Lawn from the input file
    :param input_file_path: input file path
    """
    input_file = open(input_file_path, "r")
    lawn_description_line = input_file.readline()
    lawn_width, lawn_height = map(int, lawn_description_line.split())
    lawn = Lawn(lawn_width, lawn_height)
    logger.info("Created a Lawn", lawn_width=lawn_width, lawn_height=lawn_height)
    mowers_lines = input_file.read().splitlines()
    # Group a list into chunks in Python, thanks to https://jasonstitt.com/python-group-iterator-list-function
    mowers_list = zip(*[iter(mowers_lines)] * 2)
    for initial_coordinates_and_orientation, instructions in mowers_list:
        initial_coordinate_x, initial_coordinate_y, initial_orientation = initial_coordinates_and_orientation.split()
        lawn.add_mower(int(initial_coordinate_x), int(initial_coordinate_y), initial_orientation, instructions)
        logger.info("Added a mower to the existing lawn", initial_coordinate_x=initial_coordinate_x, initial_coordinate_y=initial_coordinate_y, initial_orientation=initial_orientation)
    return lawn

def generating_output_file(output_result: dict, input_file_path: str, simulation_starting_date: datetime) -> None:
    """Generate the output file containing the result
    :param output_result: dict containing the result
    :param input_file_path: input file path
    :param simulation_starting_date: simulation starting datetime
    """
    output_file_path = input_file_path + '_output_' + simulation_starting_date.strftime("%Y%m%d%H%M%S")
    with open(output_file_path, "w") as f:
        for mower_id, mower_position in output_result.items():
            f.write(mower_position+"\n")


if __name__ == '__main__':
    if len(sys.argv) > 1:
        run(sys.argv[1])
    else:
        print('usage: app.py <inputfile>')
