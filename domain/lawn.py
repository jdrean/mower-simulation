from domain.mower import Mower

class Lawn:

    def __init__(self, size_x: int, size_y: int):
        """Create a new instance of the Lawn class
        :param size_x: Lawn width
        :param size_y: Lawn height
        """
        self.size_x = size_x
        self.size_y = size_y
        self.list_mowers = []

    def add_mower(self, initial_coordinate_x: int, initial_coordinate_y: int, initial_orientation: str, instructions: str) -> list:
        """Create a new instance of the Lawn class
        :param initial_coordinate_x: Cartesian coordinate X
        :param initial_coordinate_y: Cartesian coordinate Y
        :param initial_orientation: Mower orientation (N, S, E, W)
        :param instructions: String containing the instructions
        """
        if not self.is_inside_lawn(initial_coordinate_x, initial_coordinate_y):
            raise ValueError("The initial coordinates are outside of the lawn")
        mower = Mower(initial_coordinate_x, initial_coordinate_y, initial_orientation, instructions)
        self.list_mowers.append(mower)
        return self.list_mowers

    def is_inside_lawn(self, x: int, y: int) -> bool:
        """Return True if the coordinates (x,y) are inside the Lawn
        :param x: Cartesian coordinate X
        :param y: Cartesian coordinate Y
        """
        return (x <= self.size_x and y <= self.size_y)
