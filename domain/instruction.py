from enum import Enum

class Instruction(Enum):
    LEFT = 'L'
    RIGHT = 'R'
    FORWARD = 'F'
