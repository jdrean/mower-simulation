from domain.orientation import Orientation
from domain.instruction import Instruction

class Mower:
    """This class represents a Mower entity"""

    def __init__(self, coordinate_x: int, coordinate_y: int, orientation: str, instructions: str):
        """Create a new instance of the Mower class
        :param coordinate_x: Cartesian coordinate X
        :param coordinate_y: Cartesian coordinate Y
        :param orientation: Orientation object (N, E, W, S)
        :param instructions: String containing the instructions
        """
        self.coordinate_x = coordinate_x
        self.coordinate_y = coordinate_y
        self.orientation = Orientation(orientation)
        self.instructions = [Instruction(i) for i in instructions]

    def get_coordinates(self) -> (int, int):
        """Return a tuple containing the current coordinates of the mower"""
        return (self.coordinate_x, self.coordinate_y)

    def set_coordinates(self, new_coordinate_x: int, new_coordinate_y: int) -> (int, int):
        """Set new coordinates for the mower
        :param new_coordinate_x: Cartesian coordinate X
        :param new_coordinate_y: Cartesian coordinate Y
        """
        self.coordinate_x = new_coordinate_x
        self.coordinate_y = new_coordinate_y
        return (self.coordinate_x, self.coordinate_y)

    def turn_left(self) -> Orientation:
        """Turn the mower 90° left"""
        self.orientation.turn_left()
        return self.orientation

    def turn_right(self) -> Orientation:
        """Turn the mower 90° right"""
        self.orientation.turn_right()
        return self.orientation

    def get_forward_coordinates(self) -> (int, int):
        """Get the future coordinates of the mower after a successfull move
        :param new_coordinate_x: Cartesian coordinate X
        :param new_coordinate_y: Cartesian coordinate Y
        """
        forward_x = self.coordinate_x
        forward_y = self.coordinate_y
        if self.orientation.is_north():
            forward_y += 1
        elif self.orientation.is_south():
            forward_y -= 1
        elif self.orientation.is_east():
            forward_x += 1
        elif self.orientation.is_west():
            forward_x -= 1
        return (forward_x, forward_y)

    def dump_output(self) -> str:
        """Stringify the Mower object for the output"""
        return f"{self.coordinate_x} {self.coordinate_y} {self.orientation.cardinal_direction.value}"
