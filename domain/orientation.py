from enum import Enum

class CardinalDirection(Enum):
    NORTH = 'N'
    EAST = 'E'
    SOUTH = 'S'
    WEST = 'W'

class Orientation:
    """This class represent the orientation of a mower"""

    def __init__(self, cardinal_direction: str):
        """Create a new instance of the Orientation class
        :param cardinal_direction: the cardinal direction (N, S, E, W)
        """
        self.cardinal_direction = CardinalDirection(cardinal_direction)

    def get_cardinal_direction(self) -> str:
        """Get the cardinal direction"""
        return self.cardinal_direction.name

    def turn_left(self) -> None:
        """Turn the mower 90° left"""
        if self.is_north():
            self.cardinal_direction = CardinalDirection.WEST
        elif self.is_south():
            self.cardinal_direction = CardinalDirection.EAST
        elif self.is_east():
            self.cardinal_direction = CardinalDirection.NORTH
        elif self.is_west():
            self.cardinal_direction = CardinalDirection.SOUTH

    def turn_right(self) -> None:
        """Turn the mower 90° right"""
        if self.is_north():
            self.cardinal_direction = CardinalDirection.EAST
        elif self.is_south():
            self.cardinal_direction = CardinalDirection.WEST
        elif self.is_east():
            self.cardinal_direction = CardinalDirection.SOUTH
        elif self.is_west():
            self.cardinal_direction = CardinalDirection.NORTH

    def is_north(self) -> bool:
        """Return True if the current orientation is NORTH"""
        return self.cardinal_direction == CardinalDirection.NORTH

    def is_south(self) -> bool:
        """Return True if the current orientation is SOUTH"""
        return self.cardinal_direction == CardinalDirection.SOUTH

    def is_east(self) -> bool:
        """Return True if the current orientation is EAST"""
        return self.cardinal_direction == CardinalDirection.EAST

    def is_west(self) -> bool:
        """Return True if the current orientation is WEST"""
        return self.cardinal_direction == CardinalDirection.WEST
