import unittest

from domain.mower import Mower

class TestMower(unittest.TestCase):

    def test_get_coordinates(self):
        test_mower = Mower(coordinate_x=3, coordinate_y=3, orientation='E', instructions='FFRFFRFRRF')
        self.assertEqual(test_mower.get_coordinates(), (3,3))

    def test_set_coordinates(self):
        test_mower = Mower(coordinate_x=3, coordinate_y=3, orientation='E', instructions='FFRFFRFRRF')
        test_mower.set_coordinates(1, 2)
        self.assertEqual(test_mower.get_coordinates(), (1,2))

    def test_bad_instructions(self):
        self.assertRaises(ValueError, Mower, coordinate_x=3, coordinate_y=3, orientation='E', instructions='S')

    def test_turn_left(self):
        test_mower = Mower(coordinate_x=3, coordinate_y=3, orientation='E', instructions='FFRFFRFRRF')
        test_mower.turn_left()
        self.assertTrue(test_mower.orientation.is_north())
        self.assertFalse(test_mower.orientation.is_east())

    def test_turn_right(self):
        test_mower = Mower(coordinate_x=3, coordinate_y=3, orientation='E', instructions='FFRFFRFRRF')
        test_mower.turn_right()
        self.assertTrue(test_mower.orientation.is_south())
        self.assertFalse(test_mower.orientation.is_east())

    def test_get_forward_coordinates(self):
        test_mower = Mower(coordinate_x=3, coordinate_y=3, orientation='E', instructions='FFRFFRFRRF')
        self.assertEqual(test_mower.get_forward_coordinates(), (4,3))
        self.assertNotEqual(test_mower.get_forward_coordinates(), (3,3))
        test_mower.turn_left()
        self.assertEqual(test_mower.get_forward_coordinates(), (3,4))
        test_mower.turn_right()
        test_mower.turn_right()
        self.assertEqual(test_mower.get_forward_coordinates(), (3,2))


if __name__ == '__main__':
    unittest.main()
