import unittest

from domain.lawn import Lawn

class TestLawn(unittest.TestCase):

    def test_is_inside_lawn(self):
        lawn = Lawn(5,5)
        outsite_coordinates = (6,5)
        inside_coordinates = (5,1)
        self.assertFalse(lawn.is_inside_lawn(outsite_coordinates[0], outsite_coordinates[1]))
        self.assertTrue(lawn.is_inside_lawn(inside_coordinates[0], inside_coordinates[1]))

    def test_add_mower(self):
        lawn = Lawn(5,5)
        lawn.add_mower(initial_coordinate_x=1, initial_coordinate_y=2, initial_orientation='N', instructions='LFLFLFLFF')
        lawn.add_mower(initial_coordinate_x=3, initial_coordinate_y=3, initial_orientation='E', instructions='FFRFFRFRRF')
        self.assertEqual(len(lawn.list_mowers), 2)
        self.assertEqual(lawn.list_mowers[0].coordinate_y, 2)
        self.assertEqual(lawn.list_mowers[1].coordinate_x, 3)
        self.assertRaises(ValueError, lawn.add_mower, initial_coordinate_x=6, initial_coordinate_y=2, initial_orientation='N', instructions='LFLFLFLFF')

if __name__ == '__main__':
    unittest.main()
