import unittest

from service.simulation import run_simulation
from domain.lawn import Lawn

class TestSimulation(unittest.TestCase):

    def test_simulation(self):
        lawn = Lawn(5,5)
        lawn.add_mower(initial_coordinate_x=1, initial_coordinate_y=2, initial_orientation='N', instructions='LFLFLFLFF')
        lawn.add_mower(initial_coordinate_x=3, initial_coordinate_y=3, initial_orientation='E', instructions='FFRFFRFRRF')
        res = run_simulation(lawn)
        self.assertEqual(res, {0: '1 3 N', 1: '5 1 E'})

if __name__ == '__main__':
    unittest.main()
