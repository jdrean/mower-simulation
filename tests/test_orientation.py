import unittest

from domain.orientation import Orientation, CardinalDirection

class TestOrientation(unittest.TestCase):

    def test_unknown_orientation(self):
        self.assertRaises(ValueError, Orientation, 'R')

    def test_turn_left(self):
        orientation = Orientation('N')
        orientation.turn_left()
        self.assertTrue(orientation.is_west())
        orientation.turn_left()
        self.assertTrue(orientation.is_south())
        orientation.turn_left()
        self.assertTrue(orientation.is_east())
        orientation.turn_left()
        self.assertTrue(orientation.is_north())

    def test_turn_right(self):
        orientation = Orientation('N')
        orientation.turn_right()
        self.assertTrue(orientation.is_east())
        orientation.turn_right()
        self.assertTrue(orientation.is_south())
        orientation.turn_right()
        self.assertTrue(orientation.is_west())
        orientation.turn_right()
        self.assertTrue(orientation.is_north())


if __name__ == '__main__':
    unittest.main()
